import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { Customer } from './interfaces/customer';

@Injectable({
  providedIn: 'root'
})
export class CustomersService {

userCollection:AngularFirestoreCollection = this.db.collection('users');
customersCollection:AngularFirestoreCollection;

getCustomers(userId): Observable<any[]> {
  this.customersCollection = this.db.collection(`users/${userId}/customers`, 
     ref => ref.limit(10))
  return this.customersCollection.snapshotChanges();    
} 

updateRsult(userId:string, id:string,result:string){
  this.db.doc(`users/${userId}/customers/${id}`).update(
    {
      result:result
    })
  }

updateCustomer(userId:string, id:string,grade:number,psy:number,tuition:number){
  this.db.doc(`users/${userId}/customers/${id}`).update(
    {
      grade:grade,
      psy:psy,
      tuition:tuition,
      result:null
    }
  )
}

deleteCustomer(userId:string, id:string){
  this.db.doc(`users/${userId}/customers/${id}`).delete();
}

addCustomer(userId:string, grade:number, psy:number, tuition:number){
  const customer:Customer = {grade:grade,tuition:tuition, psy:psy}
  this.userCollection.doc(userId).collection('customers').add(customer);
} 



constructor(private db: AngularFirestore,
  ) {} 

}

