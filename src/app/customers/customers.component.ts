import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';
import { CustomersService } from '../customers.service';
import { Customer } from '../interfaces/customer';
import { PredictionService } from '../prediction.service';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.css']
})
export class CustomersComponent implements OnInit {

  userId;

  customers:Customer[];
  customers$;
  addCustomerFormOpen;
  rowToEdit:number = -1; 
  customerToEdit:Customer = {grade:null, tuition:null, psy:null};

  add(customer:Customer){
    this.customersService.addCustomer(this.userId, customer.grade, customer.psy, customer.tuition)
  }
  
  moveToEditState(index){
    console.log(this.customers[index].grade);
    this.customerToEdit.grade = this.customers[index].grade;
    this.customerToEdit.tuition = this.customers[index].tuition;
    this.customerToEdit.psy = this.customers[index].psy;
    this.rowToEdit = index; 
  }

  updateCustomer(){
    let id = this.customers[this.rowToEdit].id;
    this.customersService.updateCustomer(this.userId,id, this.customerToEdit.grade,this.customerToEdit.tuition,this.customerToEdit.psy);
    this.rowToEdit = null;
  }

  deleteCustomer(index){
    let id = this.customers[index].id;
    this.customersService.deleteCustomer(this.userId, id);
  }

  updateResult(index){
    this.customers[index].saved = true; 
    this.customersService.updateRsult(this.userId,this.customers[index].id,this.customers[index].result);
  }

  predict(index){
    this.customers[index].result = 'Will drop out';
    this.predictionService.predict(this.customers[index].tuition, this.customers[index].psy,this.customers[index].psy).subscribe(
      res => {console.log(res);
        if(res > 0.5){
          var result = 'Will pay';
        } else {
          var result = 'Will drop out'
        }
        this.customers[index].result = result}
    );  
  }

  displayedColumns: string[] = ['name', 'Education in years', 'Personal income','Delete', 'Edit', 'Predict', 'Result'];
 
  constructor(private customersService:CustomersService,
    private authService:AuthService,
    private predictionService:PredictionService ) { }

  ngOnInit(): void {
    this.authService.getUser().subscribe(
      user => {
          this.userId = user.uid;
          console.log(user.uid);
          this.customers$ = this.customersService.getCustomers(this.userId);
          this.customers$.subscribe(
            docs => {         
              this.customers = [];
              var i = 0;
              for (let document of docs) {
                console.log(i++); 
                const customer:Customer = document.payload.doc.data();
                if(customer.result){
                  customer.saved = true; 
                }
                customer.id = document.payload.doc.id;
                   this.customers.push(customer); 
              }                        
            }
          )
      })
  }   
}
