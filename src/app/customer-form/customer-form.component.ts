import { Component, OnInit, Input,  Output, EventEmitter } from '@angular/core';
import { Customer } from '../interfaces/customer';

@Component({
  selector: 'customerform',
  templateUrl: './customer-form.component.html',
  styleUrls: ['./customer-form.component.css']
})
export class CustomerFormComponent implements OnInit {
  //@Input() book: Book;
  @Input() grade: number;
  @Input() psy: number;
  @Input() id: string;
  @Input() tuition: string;
  @Input() formType: string;
  
  @Output() update = new EventEmitter<Customer>();
  @Output() closeEdit = new EventEmitter<null>();

  buttonText:String = 'Add student'; 
  
  onSubmit(){ 

  }  

  tellParentToClose(){
    this.closeEdit.emit();
  }
  
  updateParent(){
    console.log("sadas " +this.tuition );
    let a = 0;
    if(this.tuition == "true"){
        a = 1;
    }
    let customer:Customer = {id:this.id,grade:this.grade, psy:this.psy, tuition:a}; 
    this.update.emit(customer);
    if(this.formType == 'Add Book'){
      this.grade = null;
      this.psy = null;
      this.tuition = null; 
    }
  }

  constructor() { }


  ngOnInit(): void {
    if(this.formType == 'Add Book'){
      this.buttonText = 'Add';
    }
  }

}
