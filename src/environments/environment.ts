// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.


export const environment = {
  production: false,
  firebaseConfig : {
	apiKey: "AIzaSyAh5ApUL4R0XjkHvlNIZvpmGCjuFLKd2mg",
	authDomain: "lama-angular.firebaseapp.com",
	projectId: "lama-angular",
	storageBucket: "lama-angular.appspot.com",
	messagingSenderId: "838453312238",
	appId: "1:838453312238:web:592ac543b393dd4d54d1ff"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
